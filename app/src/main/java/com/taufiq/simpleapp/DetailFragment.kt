package com.taufiq.simpleapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.taufiq.simpleapp.databinding.FragmentDetailBinding
import com.taufiq.simpleapp.ui.MainAdapter
import com.taufiq.simpleapp.ui.UserViewModel
import com.taufiq.simpleapp.utils.DataState
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args: DetailFragmentArgs? by navArgs()
    private val viewModel by viewModel<UserViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        args?.id?.let { id ->
            viewModel.getUserById(id)
        } ?: run {
            Toast.makeText(requireContext(), "id is empty", Toast.LENGTH_SHORT).show()
        }

        viewModel.detailUser.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Loading -> {
                    binding.progressBar2.visibility = View.VISIBLE
                }
                is DataState.Error -> {
                    binding.progressBar2.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        "error in ${it.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is DataState.Success -> {
                    binding.progressBar2.visibility = View.GONE
                    with(binding) {
                        it.data?.let { data ->
                            Glide.with(requireActivity()).load(data.avatar).circleCrop()
                                .into(ivAvatar)
                            tvName.text = "${data.firstName} ${data.lastName}"
                            tvEmail.text = data.email
                        }

                    }
                }
            }

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}