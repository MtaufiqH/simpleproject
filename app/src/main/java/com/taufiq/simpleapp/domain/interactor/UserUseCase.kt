package com.taufiq.simpleapp.domain.interactor

import com.taufiq.simpleapp.domain.model.Users
import io.reactivex.Observable


interface UserUseCase {

    fun getUser() : Observable<List<Users>>
    fun getUserDetail(id: Int) : Observable<Users>
}