package com.taufiq.simpleapp.domain.model

data class Users(
    val id: Int,
    val email: String,
    val firstName: String,
    val lastName: String,
    val avatar: String
)