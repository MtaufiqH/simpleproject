package com.taufiq.simpleapp.domain.interactor

import com.taufiq.simpleapp.data.UserRepository
import com.taufiq.simpleapp.domain.model.Users
import com.taufiq.simpleapp.utils.Mapper.ToUserDomain
import io.reactivex.Observable

class UserInteractor(private val repository: UserRepository) : UserUseCase {

    override fun getUser(): Observable<List<Users>> {
        return repository.getUser().map { data ->
            data.map {
                it.ToUserDomain()
            }
        }


    }

    override fun getUserDetail(id: Int): Observable<Users> {
        return repository.getUserDetail(id).map {
            it.ToUserDomain()
        }
    }
}