package com.taufiq.simpleapp.di

import SchedulerProvidersImpl
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.taufiq.simpleapp.data.UserDataStore
import com.taufiq.simpleapp.data.UserRepository
import com.taufiq.simpleapp.data.remote.client.EndPoint
import com.taufiq.simpleapp.data.remote.client.UserServices
import com.taufiq.simpleapp.domain.interactor.UserInteractor
import com.taufiq.simpleapp.domain.interactor.UserUseCase
import com.taufiq.simpleapp.schedulers.SchedulerProviders
import com.taufiq.simpleapp.ui.UserViewModel
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModules = module {
    single {
        GsonBuilder()
            .setPrettyPrinting()
            .setLenient()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    single {
        provideRetrofit(get(),get())
    }
}

val appModule = module {
    factory<SchedulerProviders> { SchedulerProvidersImpl() }
    single<UserRepository> { UserDataStore(get(), get()) }
    single<UserUseCase> { UserInteractor(get()) }
    factory { provideUserApi(get()) }

    viewModel {
        UserViewModel(get(),get(),get())
    }
}

val rxModule = module {
    single {
        CompositeDisposable()
    }
}

fun provideRetrofit(okHttpClient: OkHttpClient, gsonBuilder: Gson): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(EndPoint.BASE_URL)
        .client(okHttpClient)
        .build()
}

fun provideUserApi(retrofit: Retrofit): UserServices = retrofit.create(UserServices::class.java)
