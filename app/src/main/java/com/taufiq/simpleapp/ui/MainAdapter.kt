package com.taufiq.simpleapp.ui

import android.content.Context
import com.bumptech.glide.Glide
import com.taufiq.simpleapp.R
import com.taufiq.simpleapp.databinding.UserListItemBinding
import com.taufiq.simpleapp.domain.model.Users
import com.taufiq.simpleapp.utils.BaseAdapter

class MainAdapter(private val context: Context, private val listener: (Users) -> Unit) : BaseAdapter<Users>(
    R.layout.user_list_item) {

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val userData = data[position]
        val binding = UserListItemBinding.bind(holder.itemView)
        with(binding) {
            Glide.with(context)
                .load(userData.avatar)
                .circleCrop()
                .into(binding.ivAvatar)

            tvName.text = "${userData.firstName} ${userData.lastName}"
            tvEmail.text = userData.email

            this.root.setOnClickListener {
                listener.invoke(userData)
            }


        }

    }
}