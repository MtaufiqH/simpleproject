package com.taufiq.simpleapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taufiq.simpleapp.domain.interactor.UserUseCase
import com.taufiq.simpleapp.domain.model.Users
import com.taufiq.simpleapp.schedulers.SchedulerProviders
import com.taufiq.simpleapp.utils.DataState
import com.taufiq.simpleapp.utils.applySchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import retrofit2.HttpException

class UserViewModel(
    private val useCase: UserUseCase,
    private val schedulers: SchedulerProviders,
    private val disposable: CompositeDisposable
) : ViewModel() {

    private val _users = MutableLiveData<DataState<List<Users>>>()
    val user: LiveData<DataState<List<Users>>> = _users

    private val _detailUser = MutableLiveData<DataState<Users>>()
    val detailUser : LiveData<DataState<Users>> = _detailUser


    fun getAllUsers() {
        _users.value = DataState.Loading()
        useCase.getUser().applySchedulers(schedulers).subscribeBy(
            onNext = { users ->
                _users.value = DataState.Success(users)
            },
            onError = {
                if (it is HttpException) {
                    val errorBody = it.response()?.errorBody().toString()
                    _users.value = DataState.Error(errorBody)
                } else {
                    val error = it.localizedMessage
                    _users.value = DataState.Error(error)
                }
            }).addTo(disposable)
    }


    fun getUserById(id: Int){
        _detailUser.value = DataState.Loading()
        useCase.getUserDetail(id).applySchedulers(schedulers).subscribeBy(
            onNext = {
                     _detailUser.value = DataState.Success(it)
            },
            onError = {
                if (it is HttpException) {
                    val errorHttp = it.response()?.errorBody().toString()
                    _detailUser.value = DataState.Error(errorHttp)
                } else {
                    val error = it.localizedMessage
                    _detailUser.value = DataState.Error(error)
                }
            }
        ).addTo(disposable)

    }
}