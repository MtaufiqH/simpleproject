package com.taufiq.simpleapp.ui

import android.app.Application
import com.taufiq.simpleapp.di.appModule
import com.taufiq.simpleapp.di.networkModules
import com.taufiq.simpleapp.di.rxModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class UserApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@UserApplication)
            modules(
                networkModules,
                appModule,
                rxModule
            )
        }

    }
}