package com.taufiq.simpleapp.data

import com.taufiq.simpleapp.data.remote.UserResponse
import com.taufiq.simpleapp.data.remote.client.UserServices
import com.taufiq.simpleapp.schedulers.SchedulerProviders
import com.taufiq.simpleapp.utils.applySchedulers
import com.taufiq.simpleapp.utils.mapObservable
import io.reactivex.Observable

class UserDataStore(private val service : UserServices, private val schedulerProviders: SchedulerProviders) : UserRepository{
    override fun getUser(): Observable<List<UserResponse>> {
        return service.getUsers().applySchedulers(schedulerProviders).mapObservable {
            it
        }
    }

    override fun getUserDetail(id: Int): Observable<UserResponse>{
        return service.getUserDetail(id).applySchedulers(schedulerProviders).mapObservable { it }
    }
}