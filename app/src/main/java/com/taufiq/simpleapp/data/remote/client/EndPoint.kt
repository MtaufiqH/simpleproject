package com.taufiq.simpleapp.data.remote.client

import com.taufiq.simpleapp.data.remote.BaseResponse
import io.reactivex.Observable
import retrofit2.Response

typealias ResponseUser<T> = Observable<Response<BaseResponse<T>>>

object EndPoint {
    const val BASE_URL = "https://reqres.in/"

    object User {
        const val GET_USER = "api/users/"
    }
}