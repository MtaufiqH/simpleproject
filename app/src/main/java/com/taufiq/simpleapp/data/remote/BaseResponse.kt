package com.taufiq.simpleapp.data.remote


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class BaseResponse<T>(
    @SerializedName("page")
    val page: Int? = null,
    @SerializedName("data")
    val data: T? = null,
)