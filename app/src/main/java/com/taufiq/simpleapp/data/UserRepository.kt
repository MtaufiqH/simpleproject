package com.taufiq.simpleapp.data

import com.taufiq.simpleapp.data.remote.UserResponse
import io.reactivex.Observable

interface UserRepository {

    fun getUser(): Observable<List<UserResponse>>
    fun getUserDetail(id: Int): Observable<UserResponse>
}