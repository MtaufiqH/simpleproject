package com.taufiq.simpleapp.data.remote.client

import com.taufiq.simpleapp.data.remote.UserResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface UserServices {

    @GET(EndPoint.User.GET_USER)
    fun getUsers():ResponseUser<List<UserResponse>>

    @GET("api/users/{id}")
    fun getUserDetail(
        @Path("id") path: Int,
        ): ResponseUser<UserResponse>


//    @GET
//    fun user(
//        @Header("x-app-version") appVersion: String,
//        @Header("Authorization") token: String,
//        @Url url: String
//    ): Single<ResponseEncrypt>
}