package com.taufiq.simpleapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.taufiq.simpleapp.databinding.FragmentMainBinding
import com.taufiq.simpleapp.domain.model.Users
import com.taufiq.simpleapp.ui.MainAdapter
import com.taufiq.simpleapp.ui.UserViewModel
import com.taufiq.simpleapp.utils.DataState
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val nav by lazy {
        findNavController()
    }

    private val viewModel by viewModel<UserViewModel>()
    private lateinit var adapter: MainAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllUsers()
        viewModel.user.observe(viewLifecycleOwner) {
            when (it) {

                is DataState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                is DataState.Error -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Success -> {
                    binding.progressBar.visibility = View.GONE
                    if (it.data?.isNotEmpty() == true) {
                        setupListUser(it.data)

                    }
                }
            }
        }

    }

    private fun setupListUser(userData: List<Users>) {
        with(binding) {
            context?.let {
                rvUser.layoutManager = LinearLayoutManager(it)
                adapter = MainAdapter(it) { data ->
                    Toast.makeText(
                        requireContext(),
                        "${data.firstName} clicked!",
                        Toast.LENGTH_SHORT
                    ).show()
                    val directions =
                        MainFragmentDirections.actionMainFragmentToDetailFragment(data.id)
                    nav.navigate(directions)
                }
            }

            adapter.setData(userData)
            rvUser.adapter = adapter

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}