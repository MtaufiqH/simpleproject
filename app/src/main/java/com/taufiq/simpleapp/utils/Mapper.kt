package com.taufiq.simpleapp.utils

import com.taufiq.simpleapp.data.remote.UserResponse
import com.taufiq.simpleapp.domain.model.Users

object Mapper {

fun UserResponse?.ToUserDomain(): Users =
    Users(
        this?.id.orNol(),
        this?.email ?: "",
        this?.firstName ?: "",
        this?.lastName ?: "",
        this?.avatar ?: ""
    )


}