package com.taufiq.simpleapp.ui

import SchedulerProvidersImpl
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.taufiq.simpleapp.domain.interactor.UserUseCase
import com.taufiq.simpleapp.domain.model.Users
import com.taufiq.simpleapp.schedulers.SchedulerProviders
import com.taufiq.simpleapp.utils.DataState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private var userViewModel: UserViewModel? = null

    @Mock
    private lateinit var userUseCase: UserUseCase

    @Mock
    private lateinit var schedulers: SchedulerProviders

    @Mock
    private lateinit var disposable: CompositeDisposable

    @Mock
    private lateinit var observer: Observer<DataState<List<Users>>>


    @Before
    fun setup() {
        schedulers = SchedulerProvidersImpl()
        userViewModel = UserViewModel(userUseCase, schedulers, disposable)
    }

    @Test
    fun `verify user is available`() {
        val sample = listOf(
            Users(
                1,
                "george.bluth@reqres.in",
                "George",
                "Bluth",
                "https://reqres.in/img/faces/1-image.jpg"
            ),
            Users(
                2,
                "janet.weaver@reqres.in",
                "Janet",
                "Weaver",
                "https://reqres.in/img/faces/2-image.jpg"
            )
        )

        `when`(userUseCase.getUser()).thenReturn(Observable.just(sample))
        userViewModel?.user?.observeForever(observer)

        Assert.assertEquals(sample, userViewModel?.getAllUsers())

    }
}